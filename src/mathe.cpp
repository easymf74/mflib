//mathe.cpp

#include "../includes/mathe.hpp"
#include <set>

namespace mf{
  bool is_prime(unsigned int num){
    if(num<4) return true;
    bool result = false;
    unsigned int div = 3;
    if (num % 2) {
      result = true;
      while( div*div <= num){
	if( !(num % div++)  ){
	  result = false;
	  break;
	} 
      }
    }
    return result;
  }


  unsigned int next_prime(unsigned int current){

    do {
      ++current;
    } while (!is_prime(current));
    return current;
  }

  std::map<unsigned int,unsigned int> prime_factors(unsigned long num){
    std::map<unsigned int,unsigned int> result;

    unsigned int candidate = 2;

    do{
    
      while ( num % candidate == 0) {
	if(result.count(candidate))
	  ++result[candidate];
	else
	  result[candidate] = 1;
	num /= candidate;
      }
      candidate = next_prime(candidate);
    
    }while( !is_prime(num));
  
    if (num > 1) {
      if (result.count(num))
	++result[num];
      else
	result[num] = 1;
    }
  
    return result;
  }

  unsigned long kgv(const std::vector<unsigned int> &in) {
    std::map<unsigned int, unsigned int> primes;
  
    for (unsigned int e : in) {

      std::map<unsigned int, unsigned int> pfc = prime_factors(e);
      for(auto p : pfc)
	if( !primes.count(p.first)  ||  primes[p.first] < p.second)
	  primes[p.first] = p.second;
    
    } // end for each in input

    unsigned long result{1};
    for(auto e: primes)
      for(unsigned int c = 0; c<e.second;++c)
	result *= e.first;

    return result;  
  }

  std::set<unsigned long> proper_divisors(unsigned long n){
    std::set<unsigned long> divisors{1};
    unsigned long d = 2;
    
    while( d * d <= n ){
      if(n % d == 0) {
	      divisors.insert(d);
	      divisors.insert(n/d);
      }
      ++d;
    }

    return divisors;
  }
  
} // namespace mf
