//time.cpp
#include "../includes/time.hpp"

namespace mf{
  
  Timer::Timer()
    :start{std::chrono::steady_clock::now()}
  {}

  Timer::Time Timer::operator()() const {
    return
      std::chrono::duration_cast<std::chrono::nanoseconds>
      (std::chrono::steady_clock::now() - start).count();
  }
  
}
