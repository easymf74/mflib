//output.cpp
#include "../includes/output.hpp"

namespace mf{
  Head::Head()
    : message("")
  {}

  Head::Head(std::string head, std::string txt)
    : headline(head), message(txt) 
  {}

  void Head::set_message(std::string txt)
  {
    message=txt;
  }
  void Head::set_head(std::string head)
  {
    headline=head;
  }

  void Head::print()
  {
    clearConsole
    unsigned int l;
    l=headline.size();
    std::string p_headline(44,' ');
    std::string stars(48,'*');
    if (l<45)
    {
      p_headline = p_headline.substr (0, (21-l/2) ) 
	+ headline 
	+ p_headline.substr(21-l/2+l, std::string::npos)
	+ " *";
    } else {
      p_headline = headline;
    }

    std::cout << stars << "\n"
	      << "* " << p_headline << "\n"
	      << stars << "\n"
	      << message << std::endl;
  }
} // namespace mf
