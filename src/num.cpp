// num.cpp

#include "../includes/num.hpp"

namespace mf{
  Num::Num()
    : s_val("0") , val({0}), negative(false)
  {}

  std::deque<unsigned short> s_to_deque(const std::string& s_val){
    std::deque<unsigned short> result;
    unsigned long long cut{};
    std::string part;
    for(
      std::string::const_reverse_iterator c = s_val.crbegin();
      c != s_val.crend();
      ++c){

      ++cut %= 4;
      if( *c < '0' || *c > '9' )
	cut += cut? -1 : 3;
      else{
	part = *c + part;
	if (!cut){
	  result.push_front(std::stoi(part)); 
	  part.clear();
	} // insert part at the front
      
      }// filter only digits 
    } // end backward for each char in s_val
  
    if ( part.size() )
      result.push_front(std::stoi(part));
    
    return result;
  }

  Num::Num(unsigned int ui) : Num(std::to_string(ui)){}

  Num::Num(int i) : Num(std::to_string(i)) {}

  Num::Num(long long l) : Num(std::to_string(l)){}

  Num::Num(const std::string& s){
    negative = s.size()? s[0] == '-': false;
    val = s_to_deque(s);
    set_s_val();
  }

  Num::Num(const char* cs) : Num(std::string(cs)) {}

  Num::Num(const Num& n) : s_val{n.s_val}, val{n.val}, negative(n.negative) {}

  Num Num::operator+(const Num& n) const{
    Num result{*this};
    Num add(n);

    return result += add;
  }

  Num Num::operator+(int i) const{
    return *this + Num(i);
  }


  Num Num::operator-(const Num& n)const{
    Num from{*this};
    Num sub(n);
  
    return from -= sub;
  }

  Num Num::operator-(int i)const{
    return *this - Num(i);
  }

  Num Num::operator*(const Num& n) const{
    Num f{n};
    return f *= *this;
  }

  Num Num::operator*(int i) const{
    return *this * Num(i);
  }

  Num Num::operator/(const Num& n) const{
    Num dividend{*this};
    Num divisor{n};
    if(divisor.val[0] == 0)
      throw "Division by 0";
  
    return dividend /= divisor;
  }

  Num Num::operator/(int i) const{
    return *this / Num(i);
  }

  Num Num::operator%(const Num& n) const{
    Num dividend{*this};
    Num divisor{n};
    if(divisor.val[0] == 0)
      throw "Division by 0";
    
    return dividend %= divisor;
  }

  Num Num::operator%(int i) const{
    return *this % Num(i);
  }


  void Num::set_s_val(){
    s_val.clear();
    for (unsigned int i=0; i<val.size(); ++i ) {
    
      std::string part = std::to_string(val[i]);
      if(i && part.size() < 4){
	int zeros_to_add = 4 - part.size();
	std::string s_zeros(zeros_to_add,'0');
	part  = s_zeros + part;
      }
      s_val.append(part);
    }//end for each part in val
  }


  Num& Num::operator+=(const Num& n){

    if (negative && !n.negative){
      negative = false;
      *this = n - *this;
    }else if (!negative &&n.negative){
      Num sub{n};
      sub.negative = false;
      *this -= sub; 
    }else{
      unsigned int mark {}, pos{};
    
      // all digits both have
      for(; pos < val.size() && pos < n.val.size(); ++pos){
	unsigned int pos_sum;
	pos_sum = val[val.size()-1-pos] + n.val[n.val.size()-1-pos] + mark;
	mark = pos_sum / 10000;
	pos_sum %= 10000;
	val[val.size()-1-pos] = pos_sum;
      } // end for all digits in this

      // for all digits this num hase more
      for(;mark && pos<val.size();++pos){
	unsigned int pos_sum = val[val.size()-1-pos] + mark;
	mark = pos_sum / 10000;
	pos_sum %= 10000;
	val[val.size()-1-pos] = pos_sum;
      }

      // for all digits the other num has more
      for (; pos < n.val.size(); ++pos) {
	unsigned int pos_sum = n.val[n.val.size()-1-pos] + mark;
	mark = pos_sum / 10000;
	pos_sum %= 10000;
	val.push_front(pos_sum);
      }

      if(mark) val.push_front(mark);

      set_s_val();

    
    } // end else both have the same sign
  
    return *this;
  }

  Num& Num::operator+=(int i){
    return *this += Num(i);
  }


  Num& Num::operator-=(const Num& n){

    if( negative && !n.negative){
      Num sub(n);
      negative = sub.negative = true;
      *this += sub;
    }else if(!negative && n.negative){
      Num sub(n);
      negative = sub.negative = false;
      *this += sub;
    }else if(negative){
      negative = false;
      *this = n - *this;
    }else{

      if (n>*this){
	*this = n - *this;
	negative = true;
      }else{
	unsigned long long min_size = n.val.size() < val.size()? n.val.size() : val.size();
	bool mark{};

	for(unsigned long long from_back = 1; from_back < min_size+1;++from_back){
	  unsigned long long minuend = val[val.size()-from_back];
	  unsigned long long subtrahend = n.val.at(n.val.size()-from_back) + mark;
	  if( (mark = subtrahend > minuend))
	    minuend += 10000;
	
	  val[val.size()-from_back] = minuend - subtrahend;
	
	} // end for lower size in minuend and subtrahend

	if(mark){
	  if (val.size() == min_size) {
	    negative = true;
	  } else {
	    while (!val.at(val.size() - min_size - 1)) 
	      val[val.size() - min_size++ - 1] = 9999;
	  
	    val[val.size() - min_size - 1] -= mark;
	  }
	}// end if mark

	while(!val.front() && val.size() ) val.pop_front();
	if( !val.size() ) val.push_back(0);

	set_s_val();
      } // this is lower
    
    } // end else subtraction

    return *this;
  }

  Num& Num::operator-=(int i){
    return *this -= Num(i);
  }


  Num& Num::operator*=(const Num& n){
    Num product;
  
    // Num bigger, smaler;
    // if(*this < n)
    //   bigger = n, smaler = *this;
    // else
    //   bigger = *this, smaler = n;
  
    unsigned int z = this->val.size();
    for(unsigned int i : this->val){
      --z;
      unsigned int z2 = n.val.size();
      for (unsigned int ni : n.val) {
	--z2;
	Num m = i * ni;
	m = ( m.s_val + std::string((z+z2)*4,'0') );
	product += m;
      }
    }
    product.negative = !negative != !n.negative;
    return *this = product;
  }

  Num& Num::operator*=(int i){
    return *this *= Num(i);
  }


  Num& Num::div(const Num& n){
    Num quotient, dq, diff(*this),factor;

    while ( diff >= n ) {
      for (dq = 1, factor = 1; factor * n <= diff; factor *= 2) {
	dq = factor;
      }
      quotient += dq;
      diff = *this - (quotient * n); 
    }

    return *this = quotient;
  }

  
  Num& Num::operator/=(const Num& n){
    std::string dividend = (*this)();
    std::string str_result{};
    std::deque<Num> products{0};
    std::string s_work_div;
    Num n_work_div;
    // init products
    Num p;
    for(unsigned int i=0;i<10;++i){
      p+=n; 
      products.push_back(p);
    }

    for(char c : dividend){
      s_work_div += c;
      n_work_div = s_work_div;
      unsigned int f;
      for(f=1;products[f]<=n_work_div && f<11;++f);
      --f;
      if( f || str_result.size()  )
	str_result+=std::to_string(f);
      n_work_div -= products[f];
      s_work_div = n_work_div();
    }
    
    return *this = str_result;
  }

  Num& Num::operator/=(int i){
    return *this /= Num(i);
  }

  Num& Num::operator%=(const Num& n){
    Num mem{*this};
   return *this -= ( mem / n ) * n;
  }

  Num& Num::operator%=(int i){
    return *this %= Num(i);
  }

  Num& Num::operator++(){
    return *this+=1;
  }

  Num Num::operator++(int){
    Num mem{*this};
    *this += 1;
    return mem;
  }

  Num& Num::operator--(){
    return *this -= 1;
  }

  Num Num::operator--(int){
    Num mem{*this};
    *this -= 1;
    return mem;
  }

  Num& Num::operator=(const Num& n){
    s_val = n.s_val;
    val = n.val;
    negative = n.negative;
    return *this;
  }

  bool Num::operator==(const Num& n) const{
    return negative == n.negative && s_val == n.s_val;
  }

  bool Num::operator==(int n) const{
    return *this == Num(n);
  }

  bool Num::operator!=(const Num& n) const{
    return !(*this == n);
  }

  bool Num::operator!=(int n) const{
    return !( *this == Num(n) );
  }

  bool Num::operator<=(const Num& n) const{
    return !(n < *this);
  }

  bool Num::operator<=(int i) const{
    return !( Num(i) < *this );
  }

  bool Num::operator>=(const Num& n) const{
    return !(*this < n);
  }

  bool Num::operator>=(int i) const{
    return !(*this < Num(i));
  }

  bool Num::operator<(const Num& n) const{

    return (
      (
	!(negative==false && n.negative)
	&&
	!(n.size() < size() && !negative)
	&&
	!(size() < n.size() && negative)
	&&
	*this != n
	)
      &&
      (
	(negative && n.negative == false)
	||
	size() < n.size()
	||
	(s_val < n.s_val && negative == false)
	||
	(s_val > n.s_val && negative)
	)
      ) ;
  
  }

  bool Num::operator<(int i) const
  {
    return *this < Num(i);
  }

  bool Num::operator>(const Num& n) const{
    return n < *this;
  }

  bool Num::operator>(int i) const{
    return Num(i) < *this;
  }

 std::string Num::operator()() const{
    std::string result;
    if(negative) result += '-';
    result += s_val;
    return result;
  }

  unsigned long long Num::size() const{
    return s_val.size();
  }


  Num::operator bool() const{
    return *this != 0;
  }

  Num Num::pow(const Num& n, bool inf) const{
    Num repeat(n);
    Num result(1);
    int max = 1000/(val.size()*2);
    if( n > max && inf ){
      result.s_val = "INFINITY";
      repeat=1;
    }
    while( repeat-- ){
      result *= *this;
    }
    return result;
  }

  Num Num::operator!() const{
    Num fac = *this;
    Num result = fac;
    while(--fac){
      result*=fac;
    }
    return result;
  }

  Num Num::digitsum() const{
    Num sum_of_digits;
    for(char c : s_val){
      if( !( c < '0' || c > '9') ){
	sum_of_digits+=(c-'0');
      }
    }
    return sum_of_digits;
  }
  
  
  std::ostream& operator<<(std::ostream& o, const Num& n){
    if(n.negative) o << '-';
    o << n.s_val;
    return o;
  }

} // end namespace mf
