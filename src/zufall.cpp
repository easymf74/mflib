//zufall.cpp
#include "../includes/zufall.hpp"
#include <ctime>
#include <cstdlib>

namespace mf{
  
  Zufall::Zufall(int von,int bis)
  // : Zufall( Vi{ {von,bis} } )
    :von{von},bis{bis}
  {
     std::srand(std::time(nullptr));
  }

  Zufall::Zufall(Vi verteiler_col){
    // Startwert zeitabhängig setzen
    generator.seed( time(nullptr) * time(nullptr) );
	
    // Vector von Verteilern
    for(auto v : verteiler_col){
      v_verteiler.push_back(distribution( v.at(0), v.at(1) ) );
    }
  }

  int Zufall::operator()(unsigned int index) {
    return  von + std::rand() % bis; //v_verteiler.at(index)(generator);
  }

  
} // namespace mf
