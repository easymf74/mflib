// mf.cpp
// Licenses: GPLv3 or higer :: https://www.gnu.org/licenses/gpl-3.0.en.html#license-text

#include "../includes/txt.hpp"

using Vi = std::vector<int>;
using Vvi = std::vector<Vi>;


namespace mf{

  VS split(const std::string& line, char seperator){
    size_t last = 0, next = 0;
    std::vector<std::string> values;
    while ( ((next = line.find(seperator, last)) != std::string::npos) ) {
      std::string part = line.substr(last, next - last);
      // mf::trim( part, std::string{1,seperator} );
      values.push_back( part  );
      last = next + 1; 

      // fast forward for all repeated seperators in between
      while ( ((next = line.find(seperator, last)) != std::string::npos) ) {
        if ( next != last){  
		break;
	}
	++last;
      }

    }
     
    values.push_back( line.substr(last) );
    return values;
  }

  void trim(std::string& s, const std::string& t){
    while(s.size() >= t.size() && s.substr(0,t.size() ) == t){
      s = s.substr( t.size() );
    }

    while(s.size() >= t.size() && s.substr( s.size() - t.size() ) == t ){
      s = s.substr(0,s.size()-t.size());
    }
  }


  /** 5497 5581 3888
      5497 5581 3888
      length => 3       1099511627776
      result_digits => "1099511627776"
      mem => 1
      part_index =>1
      sum => 5497+5497 = 10995
      summed_part => 995
      summed_part_digit = "0995"
      num => 3888, 5581, 5497
      
   */

  std::string add_from_parted_nums(const Vvi& parted_nums){
    std::string result_digit{};

    unsigned int length{};
    for(const Vi& e : parted_nums) if ( e.size() > length ) length = e.size();

    unsigned int mem{};
    for( unsigned int part_index = 0; part_index < length; ++part_index){
      unsigned int sum{};

      for(Vi num : parted_nums){
	if(part_index < num.size() )
	  sum += num[part_index];
      } // end for each parted nums
      sum += mem;
      mem = sum / 10000;
      unsigned int summed_part = sum % 10000;
      std::string summed_part_digit = std::to_string(summed_part);
      while( (summed_part_digit.size() < 4) && (part_index < length-1 || mem>0) )
	summed_part_digit = '0' + summed_part_digit;
      
      result_digit = summed_part_digit + result_digit;
    } // end for each part
  
    if(mem) result_digit = std::to_string(mem) + result_digit;
  
    return result_digit;
  }


  std::string add_digit_str(const VS& input){
    Vi digit_parts;
    Vvi digits;
    for(std::string s : input){
      while(s.size()){
	std::string part{};
	for(unsigned int i = 0; i < 4 && s.size(); ++i){
	  while( s.size() && !(s.back() >= '0' && s.back() <='9')) s.pop_back(); // strip out every non digit character
	  if (s.size()) {
	    part = s.back() + part;
	    s.pop_back();
	  }
	} // split in parts of max 4
	if(part.size()) digit_parts.push_back((std::stoi(part)));
	part.clear();
      } // end work on s
      if(digit_parts.size()) digits.push_back(digit_parts);
      digit_parts.clear();
    }// end for eache s in input

    return add_from_parted_nums(digits); // use of the function obove
  }

  
} // end namespace mf
