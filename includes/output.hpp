//output.hpp
#ifndef OUTPUT_H
#define OUTPUT_H OUTPUT_H
//#include "../systembefehle.h"
#include <iostream>
#include <string>

// clearconsole
#ifdef __unix__
#define clearConsole system("clear");
#elif __WIN32__
#define clearConsole system("cls");
#endif

namespace mf{
  
  class Head {
  public:
    Head();
    Head(std::string head, std::string txt);
    void set_message(std::string);
    void set_head(std::string);
    void print();

  private:
    std::string headline;
    std::string message;
  };
  
} // namespace mf
#endif // OUTPUT_H
