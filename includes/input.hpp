//input.hpp

#ifndef INPUT_H
#define INPUT_H INPUT_H
#include <iostream>
#include <string>
#include <limits>

namespace mf{

  class Menu
  {
    std::string txtMenu;
    std::string erlaubteEingaben;
    const short CHAR_TO_SHORT=48;
  public:
    Menu();
    Menu(std::string f, std::string eE);
    void setMenu(std::string f, std::string eE);
    char get_char() const;
    short get_short() const;
  };

  class Int_in {
    std::string message;

  public:
    Int_in();
    Int_in(std::string);
    void set_message(std::string);
    int get_int() const;
  };

} // namespace mf
#endif // INPUT_H
