//zufall.h

#ifndef zufall_h
#define zufall_h zufall_h

#include <random>
#include <vector>

using Vi = std::vector<std::vector<int>>;

namespace mf{
  
  class Zufall {
    using engine = std::default_random_engine;
    using distribution = std::uniform_int_distribution<int>;
    using v_distribution = std::vector<distribution>;

    // Zufallsgenerator definieren
    engine generator;
    distribution verteiler;
    v_distribution v_verteiler;

    int von=0;
    int bis=0;
  public:
    Zufall(int von, int bis);
    Zufall(Vi verteiler_col);
    // obj() / obj(index) gibt die Zufallszahl zurück
    int operator()(unsigned int index = 0);
  };
  
} // namespace mf

#endif // ifndef zufall_h
