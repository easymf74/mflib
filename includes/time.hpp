//time.hpp

#ifndef TIME_H
#define TIME_H  TIME_H

#include <chrono>

namespace mf{
  class Timer {
  public:
    using Time = unsigned long long;
    Timer();
    Time operator()() const;
  private:
    std::chrono::time_point<std::chrono::steady_clock> start;
  };
  
} // namespace mf

#endif //TIME_H

