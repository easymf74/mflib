// num.hpp

#ifndef NUM_HPP
#define NUM_HPP NUM_HPP

#include <string>
#include <deque>
#include <iostream>

namespace mf{

  class Num {
  public:
    Num();
    Num(unsigned int);
    Num(int);
    Num(long long);
    Num(const std::string&);
    Num(const char*);
    Num(const Num&);

    Num operator+(const Num&) const;
    Num operator+(int) const;
    Num operator-(const Num&)const;
    Num operator-(int)const;
    Num operator*(const Num&) const;
    Num operator*(int) const;
    Num operator/(const Num&) const;
    Num operator/(int) const;
    Num operator%(const Num&) const;
    Num operator%(int) const;
    Num& operator+=(const Num&);
    Num& operator+=(int);
    Num& operator-=(const Num&);
    Num& operator-=(int);
    Num& operator*=(const Num&);
    Num& operator*=(int);
    Num& operator/=(const Num&);
    Num& div(const Num&);
    Num& operator/=(int);
    Num& operator%=(const Num&);
    Num& operator%=(int);
    Num& operator++();
    Num operator++(int);
    Num& operator--();
    Num operator--(int);
    Num& operator=(const Num&);
    bool operator==(const Num&) const;
    bool operator==(int) const;
    bool operator!=(const Num&) const;
    bool operator!=(int) const;
    bool operator<=(const Num&) const;
    bool operator<=(int) const;
    bool operator>=(const Num&) const;
    bool operator>=(int) const;
    bool operator<(const Num&) const;
    bool operator<(int) const;
    bool operator>(const Num&) const;
    bool operator>(int) const;
    operator bool() const;
  
    Num pow(const Num&, bool inf=true) const;
    Num operator!() const;
    Num digitsum() const;
    // Num log();
    // Num sqrt();
    std::string operator()() const;
    unsigned long long size() const;
  
    friend
    std::ostream& operator<<(std::ostream&, const Num&);
  
  private:
    std::string s_val;
    std::deque<unsigned short> val; // 32768 -> 4 digits of Base 10 complet 9999 
    bool negative;

    void set_s_val();
  };

} // end namespace mf

#endif //NUM_HPP
