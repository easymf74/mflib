//txt.hpp

#ifndef TXT_H
#define TXT_H  TXT_H


#include <vector>
#include <string>

using VS = std::vector<std::string>;

namespace mf {

// Stringfunktionen:

/* Trennt eine Zeichenkette an einem Seperator auf.
 * Dieser ist standardmäßig ein Leerzeichen,
 * sofern als 2. Paramerter kein anderes Zeichen
 * übergeben wird.
 * Das Ergebnis wird als vector zurückgegeben.
 */
VS split(const std::string &, char seperator = ' ');

/*
 * Entfernt beim übergebenem String standardmäßig Leerzeichen,
 * vorn und hinten, sofern keine andere Zeichenkette übergeben wird,
 * die vorn oder hinten abgetrennt werden soll.
 * Dabei wird die Zeichenkette sooft abgetrennt,
 * wie sie vorn oder hinten vorkommt, so dass am Ende keine
 * solche Zeichenkette vorn oder hinten stehen bleibt.
 */
void trim(std::string &s, const std::string &t = " ");

/**
   returns the sum of all digits in a vector of strings
   as a string.
   Non digit charackters are ignored,
   so that "X1x2x3x" the same is as "123"
 */
std::string add_digit_str(const VS&);

  
} // namespace mf


#endif //TXT_H


