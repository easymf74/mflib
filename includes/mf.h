// mf.h
// Licenses: GPLv3 or higer :: https://www.gnu.org/licenses/gpl-3.0.en.html#license-text

/* Funktionssammlung:
   Hier möchte ich einige eigene Funktionen sammeln,
   die ich für wichtig erachte,
   sie aber nicht in der Standardbibliothek gefunden habe.
   Dies dient auch dazu, mit einer eigenen Bibliothek zu expeimentieren.
 */

#ifndef MF_HPP
#define MF_HPP MF_HPP		

// include my hpp's
#include "logging.hpp"
#include "zufall.hpp"
#include "input.hpp"
#include "output.hpp"
#include "txt.hpp"
#include "mathe.hpp"
#include "num.hpp"
#include "time.hpp"

#endif // MF_HPP
