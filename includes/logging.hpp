//LOGGING_HPP

/*
 * Funktionsübersicht
 * ******************
 * 
 * Logging abschalten:
 * 		vor dem #include einfügen:
 *  // Auskommentieren zum Einschalten des Logs
 * 	#define NODEBUG 
 * 
 * Alternativer Ausgabestream
 * 		wenn gewünscht, vor dem #include einfügen 
 * 		(umschalten durch Auskommentieren) 
 * 		z.Bsp:
 * 			// Standard std::cout
 * 			#define LOGTARGET std::cerr 
 * 
 * Trace-Funktionsaufruf 
 * (Bei NODEBUG wird die Funktion
 *  kommentarlos aufgerufen):
 * 	CALL(Funktionsname());
 * 		Bsp: CALL(mk_new_arr());
 * 
 * Test-Funktion aufrufen (kein Aufruf bei NODEBUG)
 * 	TEST(Funktionsname());
 * 		Bsp: TEST(testfkt());
 * 
 * Trace-Funktion betreten: 
 * 	ENTER(Funktionsname()); oder ENTER(Funktionsname); 
 * 
 * Trace-Funktion verlassen:
 * 	EXIT(Funktionsname()); oder EXIT(Funktionsname);
 * 
 * Trace-beliebige Ausgabe:
 * 	ECHO(STRING);
 * 	Bsp: (hier mit int i)
 * 		ECHO(std::to_string(i)+" hinzugefügt");
 * 
 * Zeitnahme:
 *  Starten der Zeitnahme durch 
 *    START_TIMER
 *  Stoppen der Zeitnahme durch
 *    STOP_TIMER
 * 
 */
 
#include <iostream>
#include <chrono> // Zeitnahme
#define CR std::chrono
#define MS CR::microseconds

#define CLOCK_HR CR::high_resolution_clock
#define TIME_DURATION(x) CR::duration_cast<MS>(x).count()
#define TimePoint CLOCK_HR::time_point
#define Duration CLOCK_HR::duration

#define ZEITNAHME(x) TimePoint x = CLOCK_HR::now();


#ifndef LOGTARGET
#define LOGTARGET std::cout
#endif //LOGTARGET

#ifdef NODEBUG
	// Wenn #define NODEBUG im Quellcode steht 
	// dann die Trace- und Test-Anweisungen 
	// durch nichts ersetzen, also nicht beachten
	#define TRACE(msg,file,line)
	// Testfunktion soll bei NODEBUG
	// nicht aufgerufen werden
	#define TEST(fktcall)
  // keine Zeitnahme bei NODEBUG
  #define START_TIMER
  #define STOP_TIMER
#else
  #define START_TIMER ZEITNAHME(START__TIME)
  #define STOP_TIMER ZEITNAHME(STOP__TIME) \
    Duration DIFF_TIME = STOP__TIME - START__TIME; \
    LOGTARGET << "Dauer: " \
    << TIME_DURATION(DIFF_TIME)/1000000.0 \
    << " Sekunden.\n";
    
	// Trace-Ausgabe siehe Definition unten
	#define TRACE(msg,file,line) LOGTARGET << "[TRACE] " \
	<< msg << " (at " << file << ":" << line << " )\n"
	
	// Testfunktion aufrufen
	#define TEST(fktcall) LOGTARGET << "Testfunktion <" \
		#fktcall << "> wird in " << __FILE__ \
		<< " Zeile " << __LINE__ << " aufgerufen\n";\
		fktcall;
#endif //NODEBUG

// Definition der Trace-Ausgaben:
// Enter + Exit anzeigen
#define ENTER(fkt) \
	TRACE("ENTER " #fkt, __FILE__,  __LINE__)
#define EXIT(fkt) \
	TRACE("EXIT " #fkt, __FILE__, __LINE__)
// Functionsaufruf anzeigen + in jeden Fall durchführen
#define CALL(fktcall) \
	TRACE("CALL " #fktcall, __FILE__, __LINE__); fktcall;

// Output
#define ECHO(str) TRACE(str,__FILE__, __LINE__)
