// mathe.hpp

#ifndef MATHE_H
#define MATHE_H  MATHE_H

#include <map>
#include <vector>
#include <set>

namespace mf {
  
  bool is_prime(unsigned int num);

  unsigned int next_prime(unsigned int current);

  std::map<unsigned int, unsigned int> prime_factors(unsigned long num);
  unsigned long kgv(const std::vector<unsigned int> &in);

  std::set<unsigned long> proper_divisors(unsigned long n);
  
} // namespace mf

#endif //MATHE_H

