#include "../includes/mf.h"
#include <cassert>

int main(int argc, char* argv[]){

  mf::Timer timer;
  START_TIMER
  // test split
  std::string hello{"Hello world -     this is an example."};
  VS hello_parts = mf::split(hello);
  assert(hello_parts.size() == 7);
  assert(hello_parts[0] == "Hello");
  assert(hello_parts[1] == "world");
  assert(hello_parts[2] == "-");
  assert(hello_parts[3] == "this");
  assert(hello_parts[5] == "an");
  assert(hello_parts.back() == "example.");
  hello_parts = mf::split(hello,'-');
  assert(hello_parts.size() == 2);
  assert(hello_parts.front() == "Hello world ");
  assert(hello_parts.back() == "     this is an example.");

  // test trim
  std::string to_trim = "   *** <num> 123 <num> ***   ";
  mf::trim(to_trim);
  assert(to_trim == "*** <num> 123 <num> ***");
  mf::trim(to_trim,"*");
  assert(to_trim == " <num> 123 <num> ");
  mf::trim(to_trim," <num> ");
  assert(to_trim == "123");
  mf::trim(to_trim,"1");
  assert(to_trim == "23");
  mf::trim(to_trim,"3");
  assert(to_trim == "2");

  // test add_digit_string
  assert( mf::add_digit_str( {"123","123"} ) == "246"  );
  assert( mf::add_digit_str( {"111123","123"} ) == "111246"  );
  assert( mf::add_digit_str( {"111xxx123","123"} ) == "111246"  );
  assert( mf::add_digit_str( {"_111 123?"," 123"} ) == "111246"  );
  assert( mf::add_digit_str(
	    {"111 111 111 111 111 111 111 111 111 111 111 111 111",
	     "111 111 111 111 111 111 111 111 111 111 111 111 111",
	     "111 111 111 111 111 111 111 111 111 111 111 111 111",
	     "111 111 111 111 111 111 111 111 111 111 111 111 111",
	     "222 222 222 222 222 222 222 222 222 222 222 222 222",
	     "333 333 333 333 333 333 333 333 333 333 333 333 888"} )
	  == "1000000000000000000000000000000000000554"  );
  assert( mf::add_digit_str({"xxx"}) == "");
  assert( mf::add_digit_str({"1.000.000"}) == "1000000");
  assert( mf::add_digit_str({"??",}) == "");
  assert( mf::add_digit_str({
	"8000000000000000000000000000000000000000000000000000000000000000000",
	"9000000000000000000000000000000000000000000000000000000000000000000"
      }) == "17000000000000000000000000000000000000000000000000000000000000000000");
  assert( mf::add_digit_str({"4445 ##1222 ", "4445 ##1222 " }) == "88902444");
  assert( mf::add_digit_str({"549755813888","549755813888"}) == "1099511627776");
  
  // test mathe
  // bool is_prime(unsigned int num)
  assert(mf::is_prime(1));
  assert(mf::is_prime(2));
  assert(mf::is_prime(3));
  assert(mf::is_prime(5));
  assert(!mf::is_prime(9));
  assert(mf::is_prime(11));
  assert(!mf::is_prime(12));
  assert(mf::is_prime(17));
  // unsigned int next_prime(unsigned int current);
  assert(mf::next_prime(1) == 2);
  assert(mf::next_prime(2) == 3);
  assert(mf::next_prime(3) == 5);
  assert(mf::next_prime(16) == 17);
  // std::map<unsigned int, unsigned int> prime_factors(unsigned long num)
  assert(mf::prime_factors(10).count(2));
  assert(mf::prime_factors(10).count(5));
  assert(mf::prime_factors(27).count(3));
  assert(mf::prime_factors(27).count(3));
  assert(mf::prime_factors(27)[3] == 3); // 3*3*3 = 27
  // unsigned long kgv(const std::vector<unsigned int> &in)
  assert(mf::kgv({1,2}) == 2);
  assert(mf::kgv({4,6}) == 12);
  assert(mf::kgv({4,6,16}) == 48);
  // std::set<unsigned long> proper_divisors(unsigned long n){
  std::set<unsigned long> pd = mf::proper_divisors(284L);
  assert(pd.size() == 5);
  assert(*pd.rbegin() == 142);
  assert(pd.count(71));
  pd = mf::proper_divisors(220L);
  assert(pd.size()==11);
  assert(*pd.rbegin() == 110);
  assert(pd.count(55) == 1);
  //test num  
  mf::Num a;
  assert(a == false);
  assert(++a == true);
  assert(a++ == 1);
  assert(a-2 == 0);
  mf::Num d("1234567890123456789012345678901234567890");
  assert(d- "1000000000000000000000000000000000000000" == "234567890123456789012345678901234567890");
  assert(d-           "100000000000000000000000000000" == "1234567890023456789012345678901234567890");
  assert(d- "                   912345678901234567890" == "1234567890123456788100000000000000000000");
  assert(d- "                   312345678901234567890" == "1234567890123456788700000000000000000000");
  assert(d- "                 99012345678901234567890" == "1234567890123456690000000000000000000000");
  assert(d- "                 49012345678901234567890" == "1234567890123456740000000000000000000000");
  assert(d- "                 49012345678901234567891" == "1234567890123456739999999999999999999999");
  assert(d-d==0);
  assert(a == 2);
  assert(a + mf::Num("12345678") == "12345680");
  mf::Num a2="1000_000_000_000_000_000_000_000_000_000";
  assert(a2 == "1000000000000000000000000000000");
  a*=a2;
  mf::Num b = a;
  a*=2;
  assert(a == b*2);
  assert(a/2 == b);
  assert(a/200 == b/100);
  assert(b/mf::Num("250_000_000_000_000_000_000_000_000_000") == 8);
  assert(b/mf::Num("50_000_000_000_000_000_000_000_000_000") == 40);
  assert(++a/2 == b);
  assert(++a/2 != b);
  assert(--a == b+b+1);
  assert(a/b == 2);
  assert(a>b);
  assert(a>=b);
  assert(b<a);
  assert(b<=a);
  assert(a<=a);
  assert(a>=b*2);
  mf::Num t{"1597536541235"};
  assert(t/15 = "106502436082");
  a*=-1;
  assert(b>a);
  assert(b*2 != a);
  mf::Num b2 = b, b3=b;
  assert(b2 > --b);
  assert(b2 > b--);
  assert(b2-2 == b--);
  b3+=4;
  assert( b3-- % 5 == 4 );
  assert( b3-- % 5 == 3 );
  assert( b3-- % 5 == 2 );
  assert( b3-- % 5 == 1 );
  assert( b3-- % 5 == 0 );
  assert( b3-- % 5 == 4 );
  a="10";
  assert( a.pow(2) == "100" );
  assert( a.pow(30) == "1000_000_000_000_000_000_000_000_000_000" );
  assert(a/2 == 5);
  assert (a % 2 == 0);
  assert( (!a) == "3628800");
  assert( (!a).digitsum()  == 27);
  std::cout << " >>> ALL TESTS OK <<<" << std::endl;
  std::cout << "in " << timer() << " Nanosekunden (milliardenstel)" <<std::endl;; 
  STOP_TIMER
  return 0;
}
