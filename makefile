# Makefile
# To Install i've done like the followed:
# add /usr/local/lib to /etc/ld.so.conf
# copy the libmf.so to /usr/local/lib
# run: sudo ldconfig
# To use a lib temporary, just set the $LD_LIBRARY_PATH to the path of the lib
# For Example: export LD_LIBRARY_PATH=~/.local/lib:$LD_LIBRARY_PATH

CXX = g++

SRC = $(wildcard $(addprefix src/*,cpp))))
OBJ = $(addprefix obj/, $(notdir $(addsuffix .o, $(basename $(SRC)))))

run: test/mf-test libmf.so
	@echo "run tests:"
	@test/mf-test

test/mf-test: $(OBJ) test/obj/mf_test.o
	$(CXX) $^ -o test/mf-test

libmf.so: $(OBJ)
	@echo "libmf.so erstellt."
	@echo "Am besten in .local/lib verlinken:"
	@echo "  ln -s libmfso ~/.local/lib/libmf.so"
	@echo "  export LD_LIBRARY_PATH=~/.local/lib:$LD_LIBRARY_PATH"
	@echo "Nuzung:"
	@echo "#include <mf.h>"
	@echo "g++ -L~/.local/lib -lmf -Wall main.cpp -o main"

obj/%.o: src/%.cpp includes/%.hpp 
	$(CXX) -fPIC -c -std=c++20 -Wall $< -o $@

test/obj/mf_test.o: test/mf_test.cpp includes/mf.h 
	$(CXX) -c -std=c++20 -Wall $< -o $@

clean:
	$(RM) obj/*.o
	$(RM) test/obj/*.o


